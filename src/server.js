import express from 'express'
import { mapOrder } from '*/utilities/sorts.js'

const app = express()

const hostname = 'localhost'
const port = 8024

app.get('/', (req, res) => {
  res.end('<h1>Hello 2424234234</h1>')
})

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Hello, I'm running at ${hostname}:${port}/`)
})